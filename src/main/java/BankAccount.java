
enum Account{
    SAVINGS,CURRENT
}
public class BankAccount {
        private long accNumber;
        private String customerName;
        private String city;
        private String state;
        private int pinCode;
        private  Account accountType;
       // private int balanceAmount;
        private String createdDate;
        private double accountBalance=0;
        private String status;



        public BankAccount(long accNumber,String customerName,Account accountType, String city, String state, int pinCode,  String createdDate, double accountBalance, String status) {
            this.accNumber=accNumber;
            this.customerName = customerName;
            this.accountType =accountType;
            this.city = city;
            this.state = state;
            this.pinCode = pinCode;
          //  this.balanceAmount = balanceAmount;
            this.createdDate = createdDate;

            this.accountBalance = accountBalance;
            this.status = status;
        }

    public BankAccount(long accNumber, String customerName, String city, String state, String createdDate, Account accountType) {
    }


    public long getAccNumber() {
            return accNumber;
        }


        public String getCustomerName() {
            return customerName;
        }

        public Account getAccountType() {
            return accountType;
        }


        public String getCity() {
            return city;
        }
        public int getPinCode() {

            return pinCode;
        }
        public String getState() {
            return state;
        }

       /* public double getBalanceAmount() {
            return balanceAmount;
        }*/
    public String getStatus() {
        return status;
    }



        public void deposit(double amount) {
            this.accountBalance = this.accountBalance + amount;
        }

        public void withdraw(double amount) {
            if (accountBalance >= amount) {
                accountBalance = accountBalance - amount;
            }
        }



}












